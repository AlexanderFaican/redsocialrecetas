var thinky = require('../config/thinky_init');
var type = thinky.type;
var r = thinky.r;
var Persona = thinky.createModel("Persona", {
    id: type.string(),
    external_id: type.string().default(r.uuid()), 
    apellidos: type.string(), 
    nombres: type.string(), 
    createdAt: type.date().default(r.now())
});
module.exports = Persona;
var Cuenta = require("./cuenta");
Persona.hasOne(Cuenta, "cuenta", "id", "id_persona");
