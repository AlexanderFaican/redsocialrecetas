$(document).ready(function(){
    var  estado_menu_top = 0;
    $('#btn_menu_top').click(function(){
        if(estado_menu_top == 0){
            //Cambiar esto del boton
            $('#btn_menu_top').removeClass('fa-arrow-circle-down');
            $('#btn_menu_top').addClass('fa-arrow-alt-circle-up');
            //Cambiar esto del menu y nav
            $('#login_div').addClass('active');
            $('#navbar').addClass('active');
            estado_menu_top = 1;
        }else if(estado_menu_top == 1){
            $('#btn_menu_top').removeClass('fa-arrow-alt-circle-up');
            $('#btn_menu_top').addClass('fa-arrow-circle-down');
            $('#login_div').removeClass('active');
            $('#navbar').removeClass('active');
            estado_menu_top = 0;
        }
    });
});