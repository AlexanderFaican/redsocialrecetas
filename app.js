var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var socialLoginClass = require("social-login");

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var sesionRouter = require('./routes/sesion');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(session({
  secret: 'RedSocialRecetas',
  resave: false,
  saveUninitialized: true
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/sesion', sesionRouter);

//Login
var socialLogin = new socialLoginClass({
  app: app, // ExpressJS instance
  url: 'http://localhost:3000',
  onAuth:	function(req, type, uniqueProperty, accessToken, refreshToken, profile, done) {
    findOrCreate({
      profile:	profile,        // Profile is the user's profile, already filtered to return only the parts that matter (no HTTP response code and that kind of useless data)
      property:	uniqueProperty, // What property in the data is unique: id, ID, name, ...
      type:		type            // What type of login that is: facebook, foursquare, google, ...
    }, function(user) {
      done(null, user);   // Return the user and continue
    });
  }
});

// Setup the various services:
socialLogin.use({
  facebook:	{
      settings:	{
          clientID: "333803427524521",
          clientSecret: "ae75fa74716ede0889f9916c2a2d3a6e",
          authParameters:	{
              scope: 'email'
          }
      },
      url:	{
          auth:		"/auth/facebook",           // The URL to use to login (<a href="/auth/facebook">Login with facebook</a>).
          callback: "/auth/facebook/callback",  // The Oauth callback url as specified in your facebook app's settings
          success:	'/',                        // Where to redirect the user once he's logged in
          fail:		'/auth/facebook/fail'       // Where to redirect the user if the login failed or was canceled.
      }
  },
  google:	{
      settings:	{}, // Google doesn't take any API key or API secret
      url:	{
          auth:		"/auth/google",
          callback: 	"/auth/google/callback",
          success:	'/',
          fail:		'/auth/google/fail'
      }
  },
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
